# Penzi
*by [Eva Peng](http://evapeng.com/)*

A vanilla js implementation of a UI for front end developer/engineer code at T Brand.

## Data
[API](http://np-ec2-nytimes-com.s3.amazonaws.com/dev/test/nyregion.js)

## Browser support
* Latest Chrome (desktop and mobile)
* Latest Firefox (desktop)
* Latest Safari (desktop and mobile)
* Microsoft Edge

## Animations

Simple CSS animations were created to show responsiveness, based on user actions.

* Opacity fade for article previews on hover
* Opacity fade for pagination buttons on hover
* Elevation shadow on language button toggle on hover

## Assumptions
* The API returns a wide range of content ex. ads, blogs, articles. I have chosen to filter only for articles with images.
* Some generalizations about punctuation with a limited set, only tackled punctuation in the beginning and end of words.

## Next steps
* Extract out hard-coded font styles and create reusable classes
* Separate out the rendering code more from the page logic for better readibility
* More exploration into animations

Thank you