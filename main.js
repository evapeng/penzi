/**
  * @desc NYTD handles data parsing and page rendering
  * @author Eva Peng renatusair@gmail.com
  * @required main.js
*/

/*
* Constants
*/
const image_host = 'https://static01.nyt.com/';
const posts_per_page = 5;
const footer_content = { brand: '2020 | T Brand Studio', developer: 'Code Test for Developers'}
const replacement_word = 'boinga';
/*
* Helper functions
*/
const Helpers = {
    isUpperCase(str) {
        return str === str.toUpperCase();
    },
    isLowerCase(str) {
        return str === str.toLowerCase();
    }
}

const NYTD = {
    data : {},
    isEnglish: true,
    current_page: 0,
    total_pages: 0,
    set_meta: (parameters) => {
        // Set description metadata
        const metaDescriptionHTML = document.createElement('meta');
        metaDescriptionHTML.setAttribute('name', 'description');
        metaDescriptionHTML.content = parameters.description;
        document.getElementsByTagName('head')[0].appendChild(metaDescriptionHTML);

        // Set keywords metadata
        const metaKeywordsHTML = document.createElement('meta');
        metaKeywordsHTML.setAttribute('name', 'keywords');
        metaKeywordsHTML.content = parameters.keywords;
        document.getElementsByTagName('head')[0].appendChild(metaKeywordsHTML);

        // Set title metadata
        const metaTitleHTML = document.createElement('meta');
        metaTitleHTML.setAttribute('name', 'title');
        metaTitleHTML.content = parameters.title;
        document.getElementsByTagName('head')[0].appendChild(metaTitleHTML)
    },
    toggle_language: (language) => {
        /**
        * Toggle language between martian and english
        */
       const isEnglish = language.toLowerCase() === 'english';
        if (NYTD.isEnglish === isEnglish) return; // Do nothing 
        NYTD.isEnglish = isEnglish;

        // Reset styles
        const buttons = document.getElementById('toggle-wrapper').children;
        for (const button of buttons) {
            button.classList.remove('header__button--selected');
        }

        // Add selected style
        const selectedElement = isEnglish?
        document.getElementById('toggle-english')
        :
        document.getElementById('toggle-martian');
        selectedElement.classList.add('header__button--selected');

        // Render new language
        NYTD.render_content(isEnglish? NYTD.data.english :  NYTD.data.martian);
    },
    createPreview: (article) => {
        /**
        * Create each content block
        */
       const {
            headline,
            byline,
            summary,
            url,
            formattedDate,
            images
        } = article;

        // Post container
        const linkHTML = document.createElement('a');
        linkHTML.href = url;
        linkHTML.target = '_blank';
        linkHTML.classList.add('post-preview-wrapper');
        const wrapperHTML = document.createElement('div');
        wrapperHTML.classList.add('post-preview');
        // Post date (desktop)
        const dateHTML = document.createElement('p');
        dateHTML.classList.add('post-preview__date', 'post-preview__date--desktop');
        const dateTextHTML = document.createTextNode(formattedDate);
        dateHTML.appendChild(dateTextHTML);
        // Post content container
        const contentWrapperHTML = document.createElement('div');
        contentWrapperHTML.classList.add('post-preview__content');
        // Post date (mobile)
        const dateMobileHTML = document.createElement('p');
        dateMobileHTML.classList.add('post-preview__date', 'post-preview__date--mobile');
        const dateMobileTextHTML = document.createTextNode(formattedDate);
        dateMobileHTML.appendChild(dateMobileTextHTML);
        // Post headline
        const titleHTML = document.createElement('h2'); 
        titleHTML.classList.add('content-headline');
        const titleTextHTML = document.createTextNode(headline);
        titleHTML.appendChild(titleTextHTML);
        // Post description
        const descHTML = document.createElement('p'); 
        descHTML.classList.add('content-description');
        const descTextHTML = document.createTextNode(summary);
        descHTML.appendChild(descTextHTML);
        // Post author
        const authorHTML = document.createElement('p');
        authorHTML.classList.add('content-author');
        const authorTextHTML = document.createTextNode(byline);
        authorHTML.appendChild(authorTextHTML);

        contentWrapperHTML.appendChild(dateMobileHTML);
        contentWrapperHTML.appendChild(titleHTML);
        contentWrapperHTML.appendChild(descHTML);
        contentWrapperHTML.appendChild(authorHTML);
        // Post thumbnail
        const imageWrapperHTML = document.createElement('div');
        const imageHTML = document.createElement('img');
        imageWrapperHTML.appendChild(imageHTML);
        imageWrapperHTML.classList.add('post-preview__image');

        if (images.length > 0) {
            imageHTML.src = image_host + images[0].types[5].content;
        }
        wrapperHTML.appendChild(dateHTML);
        wrapperHTML.appendChild(contentWrapperHTML);
        wrapperHTML.appendChild(imageWrapperHTML);
        linkHTML.appendChild(wrapperHTML);
        return linkHTML;
    },
    create_feed: (assets) => {
        /**
        * Create content feed
        */
       const feed = document.createElement('section');
        feed.id = 'main-feed';
        // Grab posts for pages
        const curPageAssets = assets.slice(NYTD.current_page * posts_per_page, (NYTD.current_page + 1) * posts_per_page)
        curPageAssets.forEach(asset => {
            feed.appendChild(NYTD.createPreview(asset));
        })
        return feed;
    },
    create_footer: () => {
        /**
        * Create footer
        */
       const footerHTML = document.createElement('footer');

        const footerTextBrandHTML = document.createElement('p');
        footerTextBrandHTML.innerHTML = '2020 | T Brand Studio';
        footerTextBrandHTML.classList = 'footer__brand';

        const footerTextHTMLDevHTML = document.createElement('p');
        footerTextHTMLDevHTML.innerHTML = 'Code Test for Developers';
        footerTextHTMLDevHTML.classList = 'footer__developer';

        const footerTextSpacerHTML = document.createElement('p');
        footerTextSpacerHTML.innerHTML = ' | ';
        footerTextSpacerHTML.classList = 'footer__spacer';
        
        footerHTML.appendChild(footerTextBrandHTML);
        footerHTML.appendChild(footerTextSpacerHTML);
        footerHTML.appendChild(footerTextHTMLDevHTML);

        return footerHTML;
    },
    transform_to_english: (data => {
        /**
        * Transform data to usable format for UI
        * Most of this is the same content, but format date
        */
       const { articles } = data;

       const transformedArticles = articles.map(article => {
           const {  publicationDt } = article;

           const published = new Date(publicationDt.split('T')[0]);
           const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' }
           const formattedDate = published.toLocaleDateString(undefined, dateOptions);

           return Object.assign({}, article, { formattedDate })
       })
       
       return Object.assign({}, data, { articles: transformedArticles });
    }),
    transform_to_martian: (data) => {
         /**
         * Transform data
         * Check if word is 3 characters or less
         * If more than 3 characters replace with boinga
         * Maintain punctuation and capitalization
         */
        const { articles, footerContent } = data;
        const transform_word = (word) => {
            // We'll assume anything with letters is a word
            const textRegex = new RegExp(/[a-zA-Z]+/);
            // Punctuation regexp list
            const punctuationRegex = new RegExp(/[.!?\\-]/);
            
            const isWord = textRegex.test(word);
            let replaceWith = replacement_word;

            if (isWord && word.length > 3) {
                // Handle punctuation
                if (punctuationRegex.test(word)){
                    const punctuationInfo = punctuationRegex.exec(word);
                    // Punctuation is in the beginning of the word
                    if (punctuationInfo.index === 0) {
                        replaceWith = word.substr(punctuationInfo.index, punctuationInfo.length) + replaceWith;
                    } // Add punctuation to the end of the word
                    else replaceWith = replaceWith + word.substr(punctuationInfo.index, punctuationInfo.length);
                }
                // Handle capitalization
                if (Helpers.isUpperCase(word)) return replaceWith.toUpperCase();
                // Check if all lowercase
                else if (Helpers.isLowerCase(word)) return replaceWith.toLowerCase();
                // Otherwise, we'll assume camelcase
                else return replaceWith[0].toUpperCase() + replaceWith.substr(1).toLowerCase();
            } else return word; // Do nothing
        }

        const transformedArticles = articles.map(article => {
            const {
                headline,
                summary,
                publicationDt
            } = article;

            // Parse date
            const published = new Date(publicationDt.split('T')[0]);
            const dateOptions = { year: 'numeric', month: 'long', day: 'numeric' }
            const formattedDate = published.toLocaleDateString(undefined, dateOptions);

            // Parse words to transform to martian language
            const headlineArr = headline.split(' ');
            const summaryArr = summary.split(' ');
            const dateArr = formattedDate.split(' ');
            
            // Transform words to martian language
            const newHeadlineArr = headlineArr.map(word => transform_word(word));
            const newSummaryArr = summaryArr.map(word => transform_word(word));
            const newDateArr = dateArr.map(word => transform_word(word));

            // Join words back into sentences
            return Object.assign({},
                article,
                { headline: newHeadlineArr.join(' ') },
                { summary: newSummaryArr.join(' ') },
                { formattedDate: newDateArr.join(' ') }
            )
        })
        
        return Object.assign({}, data, {articles: transformedArticles});
    },
    render_section_front: (data) => {
        let articles = [];
        data.page.content.forEach(block => {
            block.collections.forEach(collection => {
                collection.assets.forEach(asset => {
                    if (asset.type.toLowerCase() === 'article'){
                        if (asset.images.length > 0) articles.push(asset);
                    }
                })
            })
        })
       // Transform data to support Martian language
       const martianData = NYTD.transform_to_martian({articles, footer_content});
       const englishData = NYTD.transform_to_english({articles, footer_content});

       // Update total pages
       NYTD.total_pages = Math.ceil( englishData.articles.length / posts_per_page);
        
       // Update data object
       NYTD.data = {
           'english':  englishData,
           'martian': martianData
       };

       // Initialize page content
       NYTD.initialize_page_content(englishData.articles, data.page.parameters)
       // Add event listeners
       NYTD.add_event_listeners();
    },
    add_event_listeners: () => {
        const toggle_english = document.getElementById('toggle-english');
        const toggle_martian = document.getElementById('toggle-martian');
        toggle_english.addEventListener('click', () => NYTD.toggle_language('english'));
        toggle_martian.addEventListener('click', () => NYTD.toggle_language('martian'));

        const prevPageButton = document.getElementById('previous-page');
        const nextPageButton = document.getElementById('next-page');
        prevPageButton.addEventListener('click', NYTD.go_to_prev_page);
        nextPageButton.addEventListener('click', NYTD.go_to_next_page);
    },
    initialize_page_content: (data, parameters) => {
        NYTD.set_meta(parameters); // Set metadata of page
        // Render feed
        document.body.replaceChild(
            NYTD.create_feed(data),
            document.getElementById('main-feed')
        );
        // Render footer
        document.body.replaceChild(
            NYTD.create_footer(),
            document.getElementsByTagName('footer')[0]
        );
        // Render current page
        document.getElementById('current-page').innerHTML = `${NYTD.current_page + 1} of ${NYTD.total_pages}`
    },
    go_to_next_page: () => {
        /**
        * Go to next page
        */
       if (NYTD.current_page < NYTD.total_pages - 1)  NYTD.current_page++; // Check index out of bounds
       NYTD.render_content(NYTD.isEnglish? NYTD.data.english :  NYTD.data.martian);
       window.scrollTo(0, 0);
    },
    go_to_prev_page: () => {
        /**
        * Go to previous page
        */
       if (NYTD.current_page > 0) NYTD.current_page--; // Check index out of bounds
       NYTD.render_content(NYTD.isEnglish? NYTD.data.english :  NYTD.data.martian);
       window.scrollTo(0, 0);
    },
    render_content: (data) => {
        // Replace feed
        document.body.replaceChild(NYTD.create_feed(data.articles), document.getElementById('main-feed'));
        // Render current page
        document.getElementById('current-page').innerHTML = `${NYTD.current_page + 1} of ${NYTD.total_pages}`;
    }
}